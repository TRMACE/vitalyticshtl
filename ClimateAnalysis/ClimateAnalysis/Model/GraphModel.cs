﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace ClimateAnalysis.Model
{
    class GraphModel : INotifyPropertyChanged
    {

        private LineSeries curLine;
        private MeassureValues valueToShow;
        private DateTimeAxis dateAxis;
        private LinearAxis valueAxis;
        private MainWindow w;

        public DateTimeAxis DateAxis
        {
            get { return dateAxis; }
            set { dateAxis = value; OnPropertyChanged("DateAxis"); }
        }

        public LinearAxis ValueAxis
        {
            get { return valueAxis; }
            set { valueAxis = value; OnPropertyChanged("ValueAxis"); }
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { plotModel = value; OnPropertyChanged("PlotModel"); }
        }

        public GraphModel(MeassureValues valueToShow, MainWindow w)
        {
        	this.w = w;
            plotModel = new PlotModel();
            this.valueToShow = valueToShow;

            OxyColor lineColor;
            if (valueToShow == MeassureValues.TEMPERATURE)
            {
                lineColor = OxyColors.Green;
            }
            else if (valueToShow == MeassureValues.HUMIDITY)
            {
                lineColor = OxyColors.Blue;
            }
            else
            {
                lineColor = OxyColors.Orange;
            }

            curLine = new LineSeries(lineColor);
            curLine.StrokeThickness = 2;
            curLine.MarkerType = MarkerType.Diamond;
            curLine.MarkerSize = 2;
            curLine.MouseDown += curLine_TouchStarted;


            plotModel.Series.Add(curLine);
            SetUpModel();
        } 

        public event PropertyChangedEventHandler PropertyChanged;

        
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetUpModel()
        {
            DateAxis = new DateTimeAxis(AxisPosition.Bottom, "Datum", "dd/MM HH:mm") {IntervalLength = 50};
            PlotModel.Axes.Add(DateAxis);

            
            ValueAxis = new LinearAxis(AxisPosition.Left, 0) {};

            if (valueToShow == MeassureValues.TEMPERATURE)
            {
                ValueAxis.Title = "Grad";
                ValueAxis.AbsoluteMaximum = 40;
                ValueAxis.AbsoluteMinimum = 0;
                
            }
            else if (valueToShow == MeassureValues.HUMIDITY)
            {
                ValueAxis.Title = "Luftfeuchtigkeit";
            }
            else
            {
                ValueAxis.Title = "CO2";
                ValueAxis.IntervalLength = 1000;
            }

            PlotModel.Axes.Add(ValueAxis);
        }

        public void LoadData(Sensor s)
        {
            curLine.Points.Clear();
            if (valueToShow == MeassureValues.TEMPERATURE)
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Temperature));
                    plotModel.InvalidatePlot(true);
                }
                // Set y min and max
                ValueAxis.AbsoluteMaximum = s.getMaxTemperature();
                ValueAxis.AbsoluteMinimum = s.getMinTemperature();
            }
            else if (valueToShow == MeassureValues.HUMIDITY)
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Humidity));
                    plotModel.InvalidatePlot(true);
                }
                // Set y min and max
                ValueAxis.AbsoluteMaximum = 100;
                ValueAxis.AbsoluteMinimum = 0;
            }
            else
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Co2));
                    plotModel.InvalidatePlot(true);
                }
                // Set y min and max
                ValueAxis.AbsoluteMaximum = (Double)s.getMaxCO2();
                ValueAxis.AbsoluteMinimum = (Double)s.getMinCO2() - 1;
            }
            // Set x maximum
            SensorData d = s.getLastMeasure();
            DateAxis.AbsoluteMaximum = DateTimeAxis.ToDouble(d.Timestamp);
            
            // Set x minimum
            d = s.getFirstMeasure();
            DateAxis.AbsoluteMinimum = DateTimeAxis.ToDouble(d.Timestamp);
        }

        /// <summary>
        /// Fügt die neuen Werte des mitgegebenen Sensors zum Graphen hinzu.
        /// Achtung 
        /// </summary>
        /// <param name="s"></param>
        public void updateGraph(Sensor s){
            if (valueToShow == MeassureValues.TEMPERATURE)
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Temperature));
                    plotModel.InvalidatePlot(true);
                }
            }
            else if (valueToShow == MeassureValues.HUMIDITY)
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Humidity));
                    plotModel.InvalidatePlot(true);
                }
            }
            else
            {
                foreach (SensorData data in s.Data)
                {
                    curLine.Points.Add(new DataPoint(DateTimeAxis.ToDouble(data.Timestamp), data.Co2));
                    plotModel.InvalidatePlot(true);
                }
            }
        }
        
        public void curLine_TouchStarted(Object sender, EventArgs e) {
        	LineSeries s = (LineSeries) sender;
        	OxyMouseDownEventArgs args = (OxyMouseDownEventArgs) e;
        	DataPoint newP = s.InverseTransform(args.Position);
        	DateTime newD = DateTimeAxis.ToDateTime(newP.X);
        	SensorData data = MainWindow.curSensor.getDataByDatetime(newD);
        	w.updateSensorDetails(data);
        }
    }
}
