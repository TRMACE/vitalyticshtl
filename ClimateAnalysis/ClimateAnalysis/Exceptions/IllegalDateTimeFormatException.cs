﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateAnalysis.Exceptions
{
    class IllegalDateTimeFormatException : System.Exception
    {
        private String msg;

        public override String Message
        {
            get { return msg; }
        }

        public IllegalDateTimeFormatException(String msg)
        {
            this.msg = msg;
        }

        
    }
}
