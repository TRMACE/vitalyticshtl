﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateAnalysis.Exceptions
{
    class IllegalIntFormatException : System.Exception
    {
        private String msg;

        
        public override String Message {
            get { return msg; }
        }

        public IllegalIntFormatException(String msg)
        {
            this.msg = msg;
        }
    }
}
