﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateAnalysis.Exceptions
{

    class IllegalDoubleFormatException : System.Exception
    {
        private String msg;

        public override String Message
        {
            get { return msg; }
        }

        public IllegalDoubleFormatException(String msg)
        {
            this.msg = msg;
        }
    }
}
