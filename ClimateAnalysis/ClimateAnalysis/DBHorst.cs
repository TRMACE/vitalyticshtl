﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ClimateAnalysis
{
    public class DBHorst
    {

        private MySqlConnection con;
        private const string connectionString = "SERVER=prowect.com;" +
                                                "DATABASE=Tomsql1;" +
                                                "UID=Tomsql1;" +
                                                "PASSWORD=qrokowins2013;";
        private bool connectionOpen = false;

        /// <summary>
        /// Initialisiert eine neue Instanz von DBHorst.
        /// Im Konstruktor wird eine Verbindung zur mySql Datenbank aufgebaut.
        /// Um diese Verbindung wieder zu schließen muss DBHorst.CloseConnection() 
        /// aufgerufen werden.
        /// </summary>
        public DBHorst()
        {
            con = new MySqlConnection(connectionString);
            try
            {
                con.Open();
            }
            catch (MySqlException e)
            {
                throw e;
            }
            catch (TimeoutException e) 
            {
                System.Windows.MessageBox.Show("Timeout Exception in DBHorst Constructor!");
            }
            
            connectionOpen = true;
        }

        /// <summary>
        /// Schließt die momentane Verbinung zur mySql Datenbank, falls sie geöffnet ist.
        /// </summary>
        public void CloseConnection()
        {
            if (connectionOpen)
            {
                con.Close();
                connectionOpen = false;
            }
        }

        /// <summary>
        /// Öffnet die Verbindung zur mySql Datenbank, sofern diese noch nicht offen ist.
        /// </summary>
        public void OpenConnection()
        {
            if (!connectionOpen)
            {
                con.Open();
                connectionOpen = true;
            }
        }

        /// <summary>
        /// Liefert eine Liste aller Sensoren mit ihren Messdaten seit dem 
        /// gegebenen Datum zurück.
        /// Die Struktur der Liste ist in der SensorFactory beschrieben.
        /// </summary>
        /// <param name="since">Datum um die Messungen zu begrenzen.</param>
        /// <returns></returns>
        public LinkedList<LinkedList<String>> getAllSensorsSinceDate(DateTime since)
        {
            String procName = "getAllSensorsSinceDate";
            MySqlCommand cmd = new MySqlCommand(procName, con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@since", since);

            MySqlDataReader reader = cmd.ExecuteReader();

            LinkedList<LinkedList<String>> returnTable = new LinkedList<LinkedList<String>>();
            LinkedList<String> line = new LinkedList<String>();

            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    line.AddLast(reader.GetValue(i).ToString());
                }
                returnTable.AddLast(line);
                line = new LinkedList<string>();
            }
            reader.Close();
            return returnTable;
        }


        /// <summary>
        /// Liefert eine Liste mit allen Sensoren und allen ihren gemessenen Daten zurück.
        /// (Seit den letzten 100 Tagen)
        /// </summary>
        /// <returns></returns>
        public LinkedList<LinkedList<String>> getAllSensors()
        {
            String procName = "getAllSensors";
            MySqlCommand cmd = new MySqlCommand(procName, con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            MySqlDataReader reader = cmd.ExecuteReader();

            LinkedList<LinkedList<String>> returnTable = new LinkedList<LinkedList<String>>();
            LinkedList<String> line = new LinkedList<String>();

            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    line.AddLast(reader.GetValue(i).ToString());
                }
                returnTable.AddLast(line);
                line = new LinkedList<string>();
            }
            reader.Close();

            return returnTable;
        }

        public LinkedList<LinkedList<String>> getDataFromSensor(int sensorID, DateTime since)
        {
            String procName = "getDataFromSensor";
            MySqlCommand cmd = new MySqlCommand(procName, con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@sensorID", sensorID);
            cmd.Parameters.AddWithValue("@since", since);

            MySqlDataReader reader = cmd.ExecuteReader();

            LinkedList<LinkedList<String>> returnTable = new LinkedList<LinkedList<String>>();
            LinkedList<String> line = new LinkedList<String>();

            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    line.AddLast(reader.GetValue(i).ToString());
                }
                returnTable.AddLast(line);
                line = new LinkedList<string>();
            }
            reader.Close();

            return returnTable;
        }
    }
}
