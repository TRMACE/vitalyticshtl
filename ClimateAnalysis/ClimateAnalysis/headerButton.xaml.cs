﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ClimateAnalysis.Res;

namespace ClimateAnalysis
{
    /// <summary>
    /// Interaktionslogik für headerButtons.xaml
    /// </summary>
    /// 

    public enum buttonTypes { Close, Min, Max, Normalize };

    public partial class headerButton : UserControl
    {

        
        
        public ImageBrush backClose = new ImageBrush(loadBitmap(Res.Resources.X));
        public ImageBrush backMin = new ImageBrush(loadBitmap(Res.Resources.minus));
        public ImageBrush backMax = new ImageBrush(loadBitmap(Res.Resources.mx));
        public ImageBrush backNorm = new ImageBrush(loadBitmap(Res.Resources.norm));


        public static readonly DependencyProperty
            typeProperty = DependencyProperty.Register(
                           "ButtonType",
                           typeof(buttonTypes),
                           typeof(headerButton),
                           new FrameworkPropertyMetadata(buttonTypes.Close, new PropertyChangedCallback(OnTypeChanged)));


      
        public buttonTypes ButtonType
        {
            get { return (buttonTypes)base.GetValue(typeProperty); }
            set { base.SetValue(typeProperty, value);}
        }

        public static void OnTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            headerButton cur = (headerButton)d;
            if (cur.ButtonType == buttonTypes.Close)
            {
                cur.PART_icon.ImageSource = cur.backClose.ImageSource;
            }
            if (cur.ButtonType == buttonTypes.Min)
            {
                cur.PART_icon.ImageSource = cur.backMin.ImageSource;
            }
            if (cur.ButtonType == buttonTypes.Max)
            {
                cur.PART_icon.ImageSource = cur.backMax.ImageSource;
            }
            if (cur.ButtonType == buttonTypes.Normalize)
            {
                cur.PART_icon.ImageSource = cur.backNorm.ImageSource;
            }
        }

        public static BitmapSource loadBitmap(System.Drawing.Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(source.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty,
                   System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
        }

        public headerButton()
        {
            InitializeComponent();
            this.ButtonType = buttonTypes.Max;
        }

        private void PART_button_Click(object sender, RoutedEventArgs e)
        {
            if (ButtonType == buttonTypes.Close)
            {
                Environment.Exit(0);
            }
            else
            {
                MainWindow main = (MainWindow) Application.Current.MainWindow;
                if (ButtonType == buttonTypes.Min)
                {
                    main.WindowState = WindowState.Minimized;
                }
                else if (ButtonType == buttonTypes.Max)
                {
                    main.WindowState = WindowState.Maximized;
                    this.ButtonType = buttonTypes.Normalize;
                }
                else if (ButtonType == buttonTypes.Normalize)
                {
                    main.WindowState = WindowState.Normal;
                    this.ButtonType = buttonTypes.Max;
                }
            }
            

        }
    }
}
