﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ClimateAnalysis.Exceptions;

namespace ClimateAnalysis
{
    public static class SensorFactory
    {


        /// <summary>
        /// Liefert eine Instanz von Sensor aus den mitgegebenen Daten zurück.
        /// Die Daten haben folgendes Format:
        /// ID      Name       Datum                Temperatur      Luftfeuchte     CO2-Gehalt
        /// 1       Xerxes     21.1.2014 9:54       21,4            50              2400
        /// 1       Xerxes     21.1.2014 10:00      23              45              1500
        /// ....
        /// </summary>
        /// <param name="rawSensorData">Datenliste im dem oben beschriebenen Format</param>
        /// <returns></returns>
        public static Sensor createSensor(LinkedList<LinkedList<String>> rawSensorData)
        {
            Sensor ret = new Sensor();

            if (rawSensorData.Count == 0)
            {
                return ret;
            }

            //Der erste Wert in der 2D Liste ist die ID
            ret.ID = int.Parse(rawSensorData.First.Value.First.Value);  //erste vorkommende ID wird die SensorID
            ret.Name = rawSensorData.First.Value.First.Next.Value;      //erster vorkommender Name wird der Sensorname

            
            foreach(LinkedList<String> el in rawSensorData)
            {
                SensorData data = new SensorData();
                DateTime tempTimestamp;
                double tempTemperature;
                int tempHumidity;
                int tempCo2;

                string timeStamp = el.ElementAt(2);
                string temperature = el.ElementAt(3);
                string humidity = el.ElementAt(4);
                string co2 = el.ElementAt(5);

                try
                {
                    DateTime.TryParse(timeStamp, out tempTimestamp);
                    data.Timestamp = tempTimestamp;
                } catch(System.Exception){
                    throw new  IllegalDateTimeFormatException("The given DateTimeformat is not supported: " + timeStamp);
                }

                try
                {
                    double.TryParse(temperature.Replace(".", ","), out tempTemperature);
                    data.Temperature = tempTemperature;
                }
                catch (System.Exception)
                {
                    throw new IllegalDoubleFormatException("The given doubleformat is not valid: " + temperature);
                }

                try
                {
                    int.TryParse(humidity, out tempHumidity);
                    data.Humidity = tempHumidity;
                }
                catch (System.Exception)
                {
                    throw new IllegalIntFormatException("The given integerformat is not valid: " + humidity);
                }

                try
                {
                    int.TryParse(co2, out tempCo2);
                    data.Co2 = tempCo2;
                }
                catch (System.Exception)
                {
                    throw new IllegalIntFormatException("The given integerformat is not valid: " + co2);
                }

                ret.Data.AddLast(data);
            }
            return ret;
        }

        /// <summary>
        /// Erstellt aus einer Liste mehrerer Sensoren (mit Daten) Instanzen dieser Sensoren und gibt sie
        /// in einer LinkedList<Sensor> zurück.
        /// Die Daten haben folgendes Format:
        /// Sensor-ID      Name       Datum                Temperatur      Luftfeuchte     CO2-Gehalt
        /// 1              Xerxes     21.1.2014 9:54       21,4            50              2400
        /// 1              Xerxes     21.1.2014 10:00      23              45              1500
        /// 2              Bernd      21.1.2014 9:54       21,4            50              2400
        /// 2              Bernd      21.1.2014 10:00      21,4            50              2400
        /// ....
        /// </summary>
        /// <param name="rawSensorData"></param>
        /// <returns></returns>
        public static LinkedList<Sensor> createSensors(LinkedList<LinkedList<String>> rawSensorData)
        {
            bool newSensor = false;

            //Liste für die Sensoren initialisieren
            LinkedList<Sensor> sensorList = new LinkedList<Sensor>();

            //Liste für die Daten eines Sensors initialisieren
            LinkedList<LinkedList<String>> rawSensorDataExplicit = new LinkedList<LinkedList<String>>();

            //Erste SensorID abspeichern
            int curSensorID = int.Parse(rawSensorData.ElementAt(0).First.Value);

            //wird gebraucht um zwischen einzelnen Sensoren zu unterscheiden
            int lastIndex = 0;

            //Durch gegebene Liste iterieren
            for (int i = 0; i <= rawSensorData.Count(); i++)
            {
                if (i < rawSensorData.Count())
                {
                    newSensor = curSensorID != int.Parse(rawSensorData.ElementAt(i).First.Value);
                }
                else
                {
                    newSensor = i == rawSensorData.Count();
                }


                if (newSensor)
                {
                    //Liste für einen Sensor erstellen
                    for (int j = lastIndex; j < i; j++)
                    {
                        rawSensorDataExplicit.AddLast(rawSensorData.ElementAt(j));
                    }

                    //Neuen Sensor erstellen
                    sensorList.AddLast(createSensor(rawSensorDataExplicit));

                    //Liste für einen Sensor wieder löschen
                    rawSensorDataExplicit.Clear();

                    //Momentanen Stand in der Liste aktualisieren
                    if (i < rawSensorData.Count())
                    {
                        curSensorID = int.Parse(rawSensorData.ElementAt(i).First.Value);
                    }
                    

                    //LastIndex an begin vom neuen Sensor setzen
                    lastIndex = i;
                }
            }

            return sensorList;
        }
    }
}
