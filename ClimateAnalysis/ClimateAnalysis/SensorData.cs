﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateAnalysis
{
    public class SensorData
    {
        private int id;
        private DateTime timestamp;
        private double temperature;
        private int humidity;
        private int co2;

        #region Properties

        /// <summary>
        /// Gibt die ID der Sensordaten aus der Datenbank zurück oder legt sie fest.
        /// </summary>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gibt den Zeitstempel der Sensordaten zurück oder legt ihn fest.
        /// </summary>
        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        /// <summary>
        /// Gibt die gemessene Temperatur der Sensordaten zum Zeitpunkt "Timestamp" zurück oder legt sie fest.
        /// </summary>
        public double Temperature
        {
            get { return temperature; }
            set { temperature = value; }
        }


        /// <summary>
        /// Gibt die gemessene Luftfeuchtigkeit der Sensordaten zum Zeitpunkt "Timestamp" zurück oder legt sie fest.
        /// </summary>
        public int Humidity
        {
            get { return humidity; }
            set { humidity = value; }
        }

        /// <summary>
        /// Gibt den gemessenen Co2-Gehalt der Sensordaten zum Zeitpunkt "Timestamp" zurück oder legt ihn fest.
        /// </summary>
        public int Co2
        {
            get { return co2; }
            set { co2 = value; }
        }
        #endregion

        /// <summary>
        /// Default Konstruktor. Erstellt eine leere Instanz von SensorData.
        /// </summary>
        public SensorData() { }


        /// <summary>
        /// Erstellt eine Instanz von SensorData mit den angegebenen Werten.
        /// </summary>
        /// <param name="ID">ID der Sensordaten aus der Datenbank.</param>
        /// <param name="Timestamp">Zeitpunkt, an dem die Daten gemessen wurden.</param>
        /// <param name="Temperature">Gemessene Temperatur zum Zeitpunkt SensorData.Timestamp.</param>
        /// <param name="Humidity">Gemessene Luftfeuchtigkeit zum Zeitpunkt SensorData.Timestamp.</param>
        /// <param name="Co2">Gemessener Co2-Gehalt der Luft zum Zeitpunkt SensorData.Timestamp.</param>
        public SensorData(int ID, DateTime Timestamp, double Temperature, int Humidity, int Co2)
        {
            this.id = ID;
            this.timestamp = Timestamp;
            this.temperature = Temperature;
            this.humidity = Humidity;
            this.co2 = Co2;
        }
    }
}
