﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

using ClimateAnalysis.Res;
using System.Windows.Media;

namespace ClimateAnalysis
{
    /// <summary>
    /// Interaktionslogik für castoellen.xaml
    /// </summary>
    public partial class Castoellen : UserControl
    {
        private ImageBrush backgroundPressed = new ImageBrush();
        private ImageBrush backgroundNotPressed = new ImageBrush();
        private Sensor curSensor;
        private MainWindow main;

        public static readonly DependencyProperty
            nameProperty = DependencyProperty.Register(
                           "Name",
                           typeof(String),
                           typeof(Castoellen),
                           new FrameworkPropertyMetadata(null, null));

        

        public static readonly DependencyProperty
            temperatureProperty = DependencyProperty.Register(
                                  "Temperature",
                                  typeof(double),
                                  typeof(Castoellen),
                                  new FrameworkPropertyMetadata(0.0, null));
                       

        public static readonly DependencyProperty
            humidityProperty = DependencyProperty.Register(
                               "Humidity",
                               typeof(int),
                               typeof(Castoellen),
                               new FrameworkPropertyMetadata(0, null));

        public static readonly DependencyProperty
                co2Property = DependencyProperty.Register(
                              "Co2",
                              typeof(int),
                              typeof(Castoellen),
                              new FrameworkPropertyMetadata(0, null));

            

        public new String  Name
        {
            get { return (String)base.GetValue(nameProperty); }
            set { base.SetValue(nameProperty, value);}
        }

        public double Temperature
        {
            get { return (double)base.GetValue(temperatureProperty); }
            set { base.SetValue(temperatureProperty, value);}
        }

        public int Humidity
        {
            get { return (int)base.GetValue(humidityProperty); }
            set { base.SetValue(humidityProperty, value);}
        }

        public int Co2
        {
            get { return (int)base.GetValue(co2Property); }
            set { base.SetValue(co2Property, value);}
        }

        public Sensor CurrentSensor
        {
            get { return curSensor; }
        }

        public Castoellen(MainWindow main)
        {
            InitializeComponent();
            SetBindings();

            this.main = main;
            backgroundPressed.ImageSource = loadBitmap(Res.Resources.castoeel_bg_pressed);
            backgroundNotPressed.ImageSource = loadBitmap(Res.Resources.castoellen_back);
        }

        public Castoellen(Sensor s, MainWindow main)
        {
            InitializeComponent();
            SetBindings();

            this.main = main;
            backgroundPressed.ImageSource = loadBitmap(Res.Resources.castoeel_bg_pressed);
            backgroundNotPressed.ImageSource = loadBitmap(Res.Resources.castoellen_back);

            SensorData lastData = s.getLastMeasure();

            this.Name = s.Name;
            this.Temperature = lastData.Temperature;
            this.Humidity = lastData.Humidity;
            this.Co2 = lastData.Co2;
            this.curSensor = s;
        }

        private void SetBindings()
        {               
            Binding nameBinding = new Binding();
            nameBinding.Source = this;
            nameBinding.Path = new PropertyPath("Name");
            PART_NAME.SetBinding(Label.ContentProperty, nameBinding);

            Binding temperatureBinding = new Binding();
            temperatureBinding.Source = this;
            temperatureBinding.Path = new PropertyPath("Temperature");
            PART_TEMP.SetBinding(Label.ContentProperty, temperatureBinding);
            
            Binding humidityBinding = new Binding();
            humidityBinding.Source = this;
            humidityBinding.Path = new PropertyPath("Humidity");
            PART_HUMIDITY.SetBinding(Label.ContentProperty, humidityBinding);

            Binding co2Binding = new Binding();
            co2Binding.Source = this;
            co2Binding.Path = new PropertyPath("Co2");
            PART_CO2_CONC.SetBinding(Label.ContentProperty, co2Binding);
        }

        private void LeftMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Background = backgroundPressed;
            main.showSensor(curSensor);
        }

        private void LeftMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Background = backgroundNotPressed;
        }

        private void MouseLeaveEv(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.Background = backgroundNotPressed;
        }

        public static BitmapSource loadBitmap(System.Drawing.Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(source.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty,
                   System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
        }

        public int getSensorID()
        {
            return curSensor.ID;
        }

        public void Refresh(DBHorst dataQuerier)
        {
            curSensor.Refresh(dataQuerier);
            SensorData lastMeassure = curSensor.getLastMeasure();
            Temperature = lastMeassure.Temperature;
            Humidity = lastMeassure.Humidity;
            Co2 = lastMeassure.Co2;
        }

        public void Refresh(Sensor newSensor)
        {
            curSensor.Refresh(newSensor);
            SensorData lastMeassure = curSensor.getLastMeasure();
            Temperature = lastMeassure.Temperature;
            Humidity = lastMeassure.Humidity;
            Co2 = lastMeassure.Co2;
        }

        
    }
}
