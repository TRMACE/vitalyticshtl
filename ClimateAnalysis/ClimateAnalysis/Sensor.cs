﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClimateAnalysis
{
    /// <summary>
    /// Stellt einen Sensor mit seinen Messdaten dar.
    /// Der Sensor hat einen Liste von Daten "Data", welche eine Menge an 
    /// verschiedenen Messungen zu verschiedenen Zeitpunkten beinhaltet. Dies ist
    /// für die Darstellung im Graphen notwendig.
    /// </summary>
    public class Sensor
    {
        private int id;
        private String name;
        private LinkedList<SensorData> data;

        #region Properties

        /// <summary>
        /// Gibt die ID des Sensors aus der Datenbank zurück oder legt sie fest.
        /// </summary>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gibt den Namen des Sensors aus der Datenbank zurück oder legt ihn fest.
        /// </summary>
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gibt die Liste der gemessenen Daten für diesen Sensor zurück oder legt sie fest.
        /// </summary>
        public LinkedList<SensorData> Data
        {
            get { return data; }
            set { data = value; }
        }

        #endregion

        /// <summary>
        /// Erstellt einen leeren Sensor ohne Daten.
        /// </summary>
        public Sensor() 
        {
            data = new LinkedList<SensorData>();
        }

        /// <summary>
        /// Erstellt einen neuen Sensor mit den angegebenen Daten.
        /// </summary>
        /// <param name="ID">ID des Sensors aus der Datenbank.</param>
        /// <param name="Name">Name des Sensors.</param>
        /// <param name="Data">Gemessene Daten des Sensors (beinhaltet verschiedene Messungen von verschiedenen Zeitpunkten).</param>
        public Sensor(int ID, String Name, LinkedList<SensorData> Data)
        {
            this.ID = ID;
            this.Name = Name;
            this.Data = Data;
        }


        /// <summary>
        /// Sucht die letzte Messung in der Liste der Messungen und gibt sie zurück.
        /// Diese Funkion iteriert komplett über die Liste. Sie ist veraltet und sollte nur dann
        /// verwendet werden, wenn man sich nicht sicher ist, ob die Daten in der Liste nach Datum
        /// sortiert sind.
        /// </summary>
        /// <returns>SensorData</returns>
        public SensorData getLastMeassureUnordered()
        {
            SensorData ret = data.First.Value;
            for (int i = 1; i < data.Count; i++)
            {
                if (data.ElementAt(i).Timestamp.CompareTo(ret.Timestamp) > 0) //ist die momentane Messung in der Liste später als die vorherie späteste Messung?
                {
                    ret = data.ElementAt(i);
                }
            }
            return ret;
        }

        public SensorData getLastMeasure()
        {
            return data.Last.Value;
        }
        
        public SensorData getFirstMeasure()
        {
        	return data.First.Value;
        }
        
        /// <summary>
        /// Gibt die maximale Temperatur zurück.
        /// </summary>
        /// <returns></returns>
        public double getMaxTemperature()
        {
        	double maxTemp = double.MinValue;
        	foreach (SensorData elem in data) {
        		if (elem.Temperature > maxTemp)
        			maxTemp = elem.Temperature;
        	}
        	return maxTemp;
        }
        
        /// <summary>
        /// Gibt die minimale Temperatur zurück.
        /// </summary>
        /// <returns></returns>
        public double getMinTemperature()
        {
        	double minTemp = double.MaxValue;
        	foreach (SensorData elem in data) {
        		if (elem.Temperature < minTemp)
        			minTemp = elem.Temperature;
        	}
        	return minTemp;
        }
        
        /// <summary>
        /// Gibt den maximalen Co²-Gehalt zurück.
        /// </summary>
        /// <param name="dataQuerier"></param>
        public int getMaxCO2()
        {
        	int maxCO2 = int.MinValue;
        	foreach (SensorData elem in data) {
        		if (elem.Co2 > maxCO2)
        			maxCO2 = elem.Co2;
        	}
        	return maxCO2;
        }
        
        /// <summary>
        /// Gibt den minimalen Co²-Gehalt zurück.
        /// </summary>
        /// <param name="dataQuerier"></param>
        public int getMinCO2()
        {
        	int minCO2 = int.MaxValue;
        	foreach (SensorData elem in data) {
        		if (elem.Co2 < minCO2)
        			minCO2 = elem.Co2;
        	}
        	return minCO2;
        }
        
        public SensorData getDataByDatetime(DateTime d)
        {
        	SensorData resDate = null;
        	for(int i = 0; i < data.Count(); i++) {
        		if (data.ElementAt(i).Timestamp.CompareTo(d) >= 0) {
        			resDate = data.ElementAt(i);
        			break;
        		}
        	}
        	if (resDate == null)
        		resDate = data.ElementAt(data.Count() - 1);
        	return resDate;
        }

        /// <summary>
        /// Aktualisiert die gemessenen Daten.
        /// </summary>
        public void Refresh(DBHorst dataQuerier)
        {
            Sensor newSensor = SensorFactory.createSensor(dataQuerier.getDataFromSensor(ID, this.getLastMeasure().Timestamp));
            Data = MergeLists(Data, newSensor.Data);
        }

        public void Refresh(Sensor newSensor)
        {
            Data = MergeLists(Data, newSensor.Data);
        }

        
        /// <summary>
        /// Hängt an die alte Liste die neue Liste vorne an.
        /// </summary>
        /// <param name="oldList"></param>
        /// <param name="newList"></param>
        /// <returns></returns>
        private LinkedList<SensorData> MergeLists(LinkedList<SensorData> oldList, LinkedList<SensorData> newList)
        {
            foreach (SensorData element in newList)
            {
                oldList.AddLast(element);
            }
            return oldList;
        }
    }
}
