﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClimateAnalysis
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class Login : Window
    {

        String username, pwd;
        ThreadStart t1_login;
        ThreadStart t2_proglog;
        public Login()
        {
            InitializeComponent();
        }

        private void exit(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Castoellen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Castoellen_Click(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Sie haben auf die Castöllen geklickt!");
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //proglog.Visibility = Visibility.Visible;
            //proglog.IsIndeterminate = true;

                //if (userNameBox.Text != "" && passwordBox.Password != "")
                //{
                //    anmeldenbt.IsEnabled = false;
                //    username = userNameBox.Text;
                //    pwd = passwordBox.Password;
                //    //Thread t = new Thread(new ThreadStart(login));
                //    login();
                //}
                //else
                //    MessageBox.Show("Kein Benutzername oder Passwort angegeben!");

                //anmeldenbt.IsEnabled = true;

            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        public string getuser()
        {
            return username;
        }

        public void login()
        {
            if (authenticate(username, pwd, "htlwrn"))
            {
                //MainWindow m = new MainWindow(); //Weiterleitung zur app
                MainWindow main = new MainWindow();
                main.Show();
                this.Close();
                //m.Show();
            }
            else
            {
                MessageBox.Show("Falscher Benutzername oder Passwort!");
            }
            /*MainWindow main = new MainWindow();
            main.Show();
            this.Close();*/
        }

        private bool authenticate(string userName, string password, string domain)
        {
            string domainAndUsername = domain + @"\" + userName;
            bool authentic = false;
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://ipdata",
                    domainAndUsername, password);
                object nativeObject = entry.NativeObject;
                authentic = true;
            }
            catch (System.Exception)
            {
                try
                {
                    DirectoryEntry entry = new DirectoryEntry("LDAP://ipdata-mb",
                        domainAndUsername, password);
                    object nativeObject = entry.NativeObject;
                    authentic = true;
                }
                catch (System.Exception) { }
            }
            return authentic;
        }

        /*private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();

        }*/
    }
}
