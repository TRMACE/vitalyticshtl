﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClimateAnalysis
{
    /// <summary>
    /// Interaktionslogik für Help.xaml
    /// </summary>
    public partial class Help : Window
    {
        private bool maximized = false;
        public Help()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Close_Button_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void DockPanel_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Maximize_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!maximized)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
                maximized = true;
            }
            else
            {
                this.WindowState = System.Windows.WindowState.Normal;
                maximized = false;
            }
        }

        private void Minimize_Button_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
    }
}
