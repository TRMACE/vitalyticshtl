﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using OxyPlot;
using OxyPlot.Axes;
using ClimateAnalysis.Model;

namespace ClimateAnalysis
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Sensor curSensor;
        private Thickness castoellenMargin = new Thickness(0, 0, 0, 5);
        private GraphModel tempGraphModel;
        private GraphModel humGraphModel;
        private GraphModel co2GraphModel;  

        public MainWindow()
        {
            InitializeComponent();

            //Initialize Models
            tempGraphModel = new Model.GraphModel(MeassureValues.TEMPERATURE, this);
            humGraphModel = new Model.GraphModel(MeassureValues.HUMIDITY, this);
            co2GraphModel = new Model.GraphModel(MeassureValues.CO2, this);

            //Bind Models
            TemperatureView.DataContext = tempGraphModel;
            HumidityView.DataContext = humGraphModel;
            Co2View.DataContext = co2GraphModel;

            

            curSensor = null;

            

            //Fetch sensordata from Database
            try
            {
                DBHorst procExecuter = new DBHorst();
                LinkedList<LinkedList<String>> test = procExecuter.getAllSensorsSinceDate(new DateTime(2015, 1, 1));

                LinkedList<Sensor> sensors = SensorFactory.createSensors(test);

                foreach (Sensor element in sensors)
                {
                    Castoellen c = new Castoellen(element, this);
                    c.Margin = castoellenMargin;
                    SensorListing.Children.Add(c);
                }
                procExecuter.CloseConnection();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Fenster bewegen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DockPanel_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }


        /// <summary>
        /// Hilfe.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Help h = new Help();
            h.Show();
            h.Activate();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow = this;
        }

        /// <summary>
        /// Zeigt den mitgegebenen Sensor im Detailfenster an.
        /// Nicht verwenden, um den momentan angezeigten Sensor zu aktualisieren! (refreshDetails())
        /// </summary>
        /// <param name="s"></param>
        public void showSensor(Sensor s)
        {
            DetailsDockPanel.Visibility = System.Windows.Visibility.Visible;

            curSensor = s;
            SensorData lastMeasure = s.getLastMeasure();

            SensorName.Content = s.Name;
            TemperatureLabel.Content = lastMeasure.Temperature + " °C";
            HumidityLabel.Content = lastMeasure.Humidity + " %";
            CO2Label.Content = lastMeasure.Co2 + " ppm";
            lastMeasureLabel.Content = lastMeasure.Timestamp.ToString();
            NewDate.Content = "Zusammenfassung";

            String dataCountMessage;
            if (s.Data.Count > 1)
            {
                dataCountMessage = "Es sind " + s.Data.Count + " Messungen zu diesem Sensor bekannt";
            }
            else if (s.Data.Count == 1)
            {
                dataCountMessage = "Es ist 1 Messung zu diesem Sensor bekannt";
            }
            else
            {
                dataCountMessage = "Es ist noch keine Messung zu diesem Sensor bekannt";
            }

            SensorDetailsFooterLabel.Content = dataCountMessage;

            // Reset axes to also reset zoom
            tempGraphModel.PlotModel.ResetAllAxes();
            co2GraphModel.PlotModel.ResetAllAxes();
            humGraphModel.PlotModel.ResetAllAxes();
            
            tempGraphModel.LoadData(s);
            humGraphModel.LoadData(s);
            co2GraphModel.LoadData(s);

            

        }
        
        /// <summary>
        /// Aktualisiert das Detailfenster auf die Daten des ausgewählten Datapoints.
        /// </summary>
        /// <param name="d"></param>
        public void updateSensorDetails(SensorData d)
        {
        	TemperatureLabel.Content = d.Temperature + " °C";
            HumidityLabel.Content = d.Humidity + " %";
            CO2Label.Content = d.Co2 + " ppm";
            NewDate.Content = "Details am " + d.Timestamp.ToString();
        }


        /// <summary>
        /// Aktualisiert das Detailfenster auf die neusten Daten des momentanen Sensors.
        /// Diese Funktion bezieht keine Daten aus der Datenbank! Sie ist nur zu verwenden, um das
        /// Detailsfenster des momentan angezeigten Sensors zu aktualieren.
        /// </summary>
        public void refreshDetails()
        {
            SensorData lastMeasure = curSensor.getLastMeasure();

            SensorName.Content = curSensor.Name;
            TemperatureLabel.Content = lastMeasure.Temperature + " °C";
            HumidityLabel.Content = lastMeasure.Humidity + " %";
            CO2Label.Content = lastMeasure.Co2 + " ppm";
            lastMeasureLabel.Content = lastMeasure.Timestamp.ToString();

            String dataCountMessage;
            if (curSensor.Data.Count > 1)
            {
                dataCountMessage = "Es sind " + curSensor.Data.Count + " Messungen zu diesem Sensor bekannt";
            }
            else if (curSensor.Data.Count == 1)
            {
                dataCountMessage = "Es ist 1 Messung zu diesem Sensor bekannt";
            }
            else
            {
                dataCountMessage = "Es ist noch keine Messung zu diesem Sensor bekannt";
            }



            SensorDetailsFooterLabel.Content = dataCountMessage;
        }

        /// <summary>
        /// Export.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            Export exportDialog = new Export(this);
            exportDialog.ShowActivated = true;
            exportDialog.Show();
        }

        /// <summary>
        /// Refresh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DBHorst dataQuerier = new DBHorst();
            Sensor oldSensor, newSensor;
            foreach (Castoellen c in SensorListing.Children)
            {
                oldSensor = c.CurrentSensor;
                newSensor = SensorFactory.createSensor(dataQuerier.getDataFromSensor(oldSensor.ID, oldSensor.getLastMeasure().Timestamp));
                c.Refresh(newSensor);
                if(oldSensor == curSensor)
                {
                    tempGraphModel.updateGraph(newSensor);
                    humGraphModel.updateGraph(newSensor);
                    co2GraphModel.updateGraph(newSensor);
                }
            }
            

            //Es wird erst die Detailanzeige aktualisiert, wenn sie angezeigt wird.
            if (curSensor != null)
            {
                refreshDetails();
            }

            dataQuerier.CloseConnection();

        }

        private void Tabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        //private void forward_click(object sender, RoutedEventArgs e)
        //{
        //    DateTimeAxis axis = TemperatureView.Model.Axes[0] as DateTimeAxis;

        //    axis.Minimum = 

        //    DateTime dtMax = DateTime.FromOADate(axis.ActualMaximum);
        //    DateTime dtMin = DateTime.FromOADate(axis.ActualMinimum);
            

        //    String s = "";
        //    s += "ActualMaximum: " + dtMax + "\n";
        //    s += "ActualMinimum: " + dtMin + "\n";
        //    s += "Maximum: " + TemperatureView.Model.Axes[0].Maximum.ToString() + "\n";
        //    MessageBox.Show(s);

        //    //TemperatureView.Model.Axes[0].Zoom(300, 600);
        //    //TemperatureView.Model.InvalidatePlot(true);
            
        //}

        

        

    }
}
