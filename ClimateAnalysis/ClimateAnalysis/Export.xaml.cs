﻿/*
 * Created by SharpDevelop.
 * User: Silverclaw
 * Date: 11/27/2014
 * Time: 08:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;

namespace ClimateAnalysis
{
	/// <summary>
	/// Interaction logic for Export.xaml
	/// </summary>
	public partial class Export : Window
	{
		public MainWindow mw;
		public Export()
		{
			InitializeComponent();
		}
		
		public Export(MainWindow mw){
			this.mw = mw;
			InitializeComponent();
			for(int i = 0; i < mw.SensorListing.Children.Count;i++){
				System.Windows.Controls.CheckBox chk = new System.Windows.Controls.CheckBox();
				Castoellen ca = (Castoellen)mw.SensorListing.Children[i];
				chk.Content = ca.Name;
				SensorList.Items.Add(chk);
			}
		}
		
		void Exportieren(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
			dlg.FileName = "Export"; // Default file name
			dlg.DefaultExt = ".csv"; // Default file extension
			dlg.Filter = "Text (.csv)|*.csv"; // Filter files by extension
			dlg.ShowDialog();
			StreamWriter filewrite = new StreamWriter(dlg.FileName);
			Castoellen ca; 
			filewrite.WriteLine("NAME;TEMPERATUR C;LUFTFEUCHTIGKEIT %;CO2 ppm;");
			for(int i = 0; i < SensorList.Items.Count;i++){
				System.Windows.Controls.CheckBox item = (CheckBox)SensorList.Items[i];
				if(item.IsChecked == true){
					ca = (Castoellen)mw.SensorListing.Children[i];
					String output = ca.Name+";";
					if(Temperature.IsChecked == true){
						output+=ca.Temperature+";";
					}
					if(Luftfeuchtigkeit.IsChecked == true){
						output+=ca.Humidity+";";
					}
					if(CO.IsChecked == true){
						output+=ca.Co2+";";
					}
					filewrite.WriteLine(output);
				}
			}
			filewrite.Close();
		}
	}
}