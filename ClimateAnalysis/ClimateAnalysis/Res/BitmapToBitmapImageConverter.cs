﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace ClimateAnalysis.Res
{

    [ValueConversion(typeof(System.Drawing.Bitmap), typeof(System.Windows.Media.Imaging.BitmapSource))]
    class BitmapToBitmapImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            System.Drawing.Bitmap bmp = (System.Drawing.Bitmap)value;
            BitmapSource ScreenCapture = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
   bmp.GetHbitmap(),
   IntPtr.Zero,
   System.Windows.Int32Rect.Empty,
   BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
            return ScreenCapture;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
