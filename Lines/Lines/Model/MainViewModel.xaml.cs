﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OxyPlot;

/*
 * Dokumentation :
 * 
 * http://oxyplot.org/documentation/
 */



namespace Lines.Model
{
    /// <summary>
    /// Interaktionslogik für MainViewModel.xaml
    /// </summary>
    public partial class MainViewModel : Window
    {
        public MainViewModel()
        {
            this.Title = "Climate Active";
            //Temperatur
            this.C = new List<DataPoint>
                              {
                                  new DataPoint(0, 20),
                                  new DataPoint(1, 22),
                                  new DataPoint(2, 18),
                                  new DataPoint(3, 19),
                                  new DataPoint(4, 24),
                                  new DataPoint(5, 22),
                                  new DataPoint(6,20),
                                  new DataPoint(7,18)
                              };

            //CO2 gehalt // ppm
            this.Co2 = new List<DataPoint>
                              {
                                  new DataPoint(1, 1500),
                                  new DataPoint(2, 3000),
                                  new DataPoint(3, 800),
                                  new DataPoint(4, 1400),
                                  new DataPoint(5, 1000),
                                  new DataPoint(6, 890),
                                  new DataPoint(7,2100)
                              };
            //Luftfeuchtigkeit in %
            this.Luftf = new List<DataPoint>
                              {
                                  new DataPoint(1, 40),
                                  new DataPoint(2, 52),
                                  new DataPoint(3, 62),
                                  new DataPoint(4, 42),
                                  new DataPoint(5, 39),
                                  new DataPoint(6, 55),
                                  new DataPoint(7,65)
                              };

            InitializeComponent();
        }
        public string Title { get; private set; }

        public IList<DataPoint> C { get; private set; }
        public IList<DataPoint> Co2 { get; private set; }
        public IList<DataPoint> Luftf { get; private set; }

        
    }
}
