﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lines.LineKlassen
{
    public class Temperatur
    {
        public List<int> Celsius;
        public List<int> Time;


        public Temperatur(int c,int t)
        {
            Celsius.Add(c);
            Time.Add(t);
        }
    }
}
