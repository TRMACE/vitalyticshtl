use Tomsql1;

DELIMITER $$

drop procedure getAllSensorsSinceDate;
$$

#Diese Procedure gibt alle Messdaten, welche zeitlich nach dem
#mitgegebenen Datum gemessen wurden, aller Sensoren zurück.
create procedure getAllSensorsSinceDate(since Date)
begin
	select s.sensor_id, s.sensor_name, d.date, d.temperature, d.humidity, d.co2_content 
	from Sensors s
		join (select * from Data where date > since) d on s.sensor_id = d.sensor_id
	order by s.sensor_id, d.date;
end;
$$

Delimiter ;

call getAllSensorsSinceDate('2015-01-25 00:00:00');

