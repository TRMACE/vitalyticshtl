CREATE DATABASE `climatedb` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `Sensors` (
  `sensor_id` int(11) NOT NULL ,
  `sensor_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sensor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `temperature` decimal(5,3) DEFAULT NULL,
  `humidity` int(11) DEFAULT NULL,
  `co2_content` int(11) DEFAULT NULL,
  `sensor_id` int(11) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `fk_Data_1_idx` (`sensor_id`),
  CONSTRAINT `fk_Data_1` FOREIGN KEY (`sensor_id`) REFERENCES `Sensors` (`sensor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
