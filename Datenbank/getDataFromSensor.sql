use Tomsql1;

DELIMITER $$

drop procedure getDataFromSensor;
$$

#Diese Procedure gibt alle Messdaten aller Sensoren der letzten
#100 Tage zurück
create procedure getDataFromSensor(sensorID integer, since datetime)
begin
	select s.sensor_id, s.sensor_name, d.date, d.temperature, d.humidity, d.co2_content 
	from Data d 
		join Sensors s on s.sensor_id = d.sensor_id
	where d.date > since and s.sensor_id = sensorID
	order by s.sensor_id, d.date;
end;
$$

DELIMITER ;

call getDataFromSensor(1, '2015-01-15');
