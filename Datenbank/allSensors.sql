use Tomsql1;

DELIMITER $$

drop procedure getAllSensors;
$$

#Diese Procedure gibt alle Messdaten aller Sensoren der letzten
#100 Tage zurück
create procedure getAllSensors()
begin
	select s.sensor_id, s.sensor_name, d.date, d.temperature, d.humidity, d.co2_content 
	from Data d 
		join Sensors s on s.sensor_id = d.sensor_id
	where d.date > date_sub(now(), interval 100 day)
	order by s.sensor_id, d.date;
end;
$$

DELIMITER ;

call getAllSensors();
