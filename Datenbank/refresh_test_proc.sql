DELIMITER $$

drop procedure test_refresh;
$$

create procedure test_refresh(temp int, hum int, co2 int)
begin
	declare sensor_id int;
	set sensor_id = 1;
	whatever: loop
		insert into Data (date, temperature, humidity, co2_content, sensor_id) values (now(), temp, hum, co2, sensor_id);
		set sensor_id = sensor_id + 1;
		if(sensor_id = 5) then
			leave whatever;
		end if;
	end loop whatever;
end;
$$

call test_refresh(-100, 31, 31);

select * from Sensors join Data on Data.sensor_id = Sensors.sensor_id;

select * from Data;