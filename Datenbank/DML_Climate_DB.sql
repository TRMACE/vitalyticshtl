DELETE FROM `climatedb`.`Sensors`;
DELETE FROM `climatedb`.`Data`;

-- Sensors

INSERT INTO `climatedb`.`Sensors` (`sensor_id`, `sensor_name`) VALUES (1,'Zeus');
INSERT INTO `climatedb`.`Sensors` (`sensor_id`, `sensor_name`) VALUES (2,'Hermes');
INSERT INTO `climatedb`.`Sensors` (`sensor_id`, `sensor_name`) VALUES (3,'Ares');
INSERT INTO `climatedb`.`Sensors` (`sensor_id`, `sensor_name`) VALUES (4,'Athene');
INSERT INTO `climatedb`.`Sensors` (`sensor_id`, `sensor_name`) VALUES (5,'Herakles');

select * from `climatedb`.`Sensors`;

-- Data

-- Sensor 1

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (current_date, 22.7, 7, 5, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 12 minute)  , 24.6, 5, 3, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 1 day)  , 22.6, 7, 4, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 55 minute)  , 24.6, 6, 6, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 66 minute)  , 18.6, 5, 5, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 88 minute)  , 16.6, 12, 7, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 88 minute)  , 16.6, 12, 7, 1);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 155 minute)  , 22.6, 7, 4, 1);


-- Sensor 2

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (current_date, 22.7, 7, 5, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 12 hour)  , 24.6, 5, 3, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 18 minute)  , 22.6, 7, 4, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 1 day)  , 24.6, 6, 6, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 16.6, 12, 7, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 3 day)  , 16.6, 12, 7, 2);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 155 minute)  , 22.6, 7, 4, 2);


-- Sensor 3


INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (current_date, 22.7, 7, 5, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 12 hour)  , 24.6, 5, 3, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 22 minute)  , 22.6, 7, 4, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 24.6, 6, 6, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 month)  , 16.6, 12, 7, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 3 day)  , 16.6, 12, 7, 3);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 155 second)  , 22.6, 7, 4, 3);


-- Sensor 4

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (current_date, 22.7, 7, 5, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 12 hour)  , 24.6, 5, 3, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 13 minute)  , 14.6, 7, 4, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 24.6, 6, 6, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 month)  , 15.6, 14, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 16.6, 17, 7, 4);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 185 second)  , 22.6, 7, 4, 4);


-- Sensor 5

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (current_date, 22.7, 7, 5, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 12 hour)  , 23.6, 5, 3, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 13 minute)  , 14.6, 7, 2, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 24.6, 6, 6, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 month)  , 15.6, 14, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 2 day)  , 22.6, 17, 7, 5);

INSERT INTO `climatedb`.`Data`
(`date`,`temperature`,`humidity`,`co2_content`,`sensor_id`)
VALUES (date_add(current_date,interval 185 second)  , 22.6, 7, 4, 5);


select * from `climatedb`.`Data`;

commit;


select * from `climatedb`.`Sensors`;
select * from `climatedb`.`Data`;
